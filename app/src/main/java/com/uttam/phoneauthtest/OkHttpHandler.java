package com.uttam.phoneauthtest;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OkHttpHandler {
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    OkHttpClient client = new OkHttpClient();

    String requestPOST(String url, String json) throws IOException {
        Log.d("TAG", "handleSignIn: requestPOST: url: "+url+", jsonstring: " + json );
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
//            Log.d("TAG", "handleSignIn: requestPOST: response: "+response );
            return response.toString();
        }
    }

}