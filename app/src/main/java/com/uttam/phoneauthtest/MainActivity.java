package com.uttam.phoneauthtest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONObject;
import java.util.Arrays;

//added with  ukumariucse@gmail.com firebase console phoneauthtest project
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 1;
    private EditText emailEditField, passwordEditField;
    private BottomSheetDialog dialog;

    //facebook
    private CallbackManager mCallbackManager;
//    public static final MediaType JSON = MediaType.get("application/x-www-form-urlencoded");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        assignViews();
        mAuth = FirebaseAuth.getInstance();

        //got from google-services.json file
        String Client_id = getResources().getString(R.string.client_id);

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Client_id)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        //facebook sdk initialization
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d("TAG", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            user.getIdToken(true).addOnSuccessListener(result -> {
                                String mUserToken = result.getToken();
                                //Do whatever
                                Log.d("TAG", "GetTokenResult result = " + mUserToken);
                                JSONObject jsObj = new JSONObject();
                                try {
                                    jsObj.put("name", user.getDisplayName());
                                    jsObj.put("email", user.getEmail());
                                    jsObj.put("phone", user.getPhoneNumber());
                                    jsObj.put("token", mUserToken);
                                    jsObj.put("photo", user.getPhotoUrl());
                                    jsObj.put("uid", user.getUid());
                                } catch (Exception e) {
                                    Log.d("TAG", "onComplete: " + e.getMessage());
                                }
                                Intent intent = new Intent(MainActivity.this, PasswordSettingsActivity.class);
                                intent.putExtra("userdata", jsObj.toString());
                                startActivity(intent);
                                finish();
                            });
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    private void assignViews() {
        emailEditField = findViewById(R.id.editPhoneEmailId);
        passwordEditField = findViewById(R.id.editPasswordId);
    }

    public void handleSignIn(View v) {
        Log.d("TAG", "handleSignIn: clicked");
    }

    public void handleSignUp(View v) {
        openSignInOptions();
    }

    //clicked setting icon when app is in onStream
    private void openSignInOptions() {
        dialog = new BottomSheetDialog(MainActivity.this);
        LayoutInflater layoutInflater = this.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.signup_options_view, null);
        dialog.setContentView(view);
        //view assign
        TextView handlePhoneSignUp = view.findViewById(R.id.handlePhoneSignUpId);
        handlePhoneSignUp.setOnClickListener(this);

        TextView handleEmailSignUp = view.findViewById(R.id.handleEmailSignUpId);
        handleEmailSignUp.setOnClickListener(this);

        TextView handleGoogleSignUp = view.findViewById(R.id.handleGoogleSignUpId);
        handleGoogleSignUp.setOnClickListener(this);

        TextView handleFacebookSignUP = view.findViewById(R.id.handleFacebookSignUPId);
        handleFacebookSignUP.setOnClickListener(this);
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.handlePhoneSignUpId:
            case R.id.handleEmailSignUpId:
                dialog.dismiss();
                Intent intent = new Intent(MainActivity.this, EmailPhoneAuthActivity.class);
                String option = v.getId() == R.id.handlePhoneSignUpId ? "phone" : "email";
                intent.putExtra("sign_in_option", option);
                startActivity(intent);
                break;
            case R.id.handleGoogleSignUpId:
                dialog.dismiss();
                intent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(intent, RC_SIGN_IN);
                break;
            case R.id.handleFacebookSignUPId:
                dialog.dismiss();
                firebaseAuthWithFacebook();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
//        if (user != null) {
//            user.reload();
//            mAuth.signOut();
//            user.delete();
//        }

//        FirebaseUser currentUser = mAuth.getCurrentUser();
//        if(currentUser != null ){
//            List<? extends UserInfo> infos = currentUser.getProviderData();
//            for (UserInfo ui : infos) {
//                Log.d("TAG", "onStart_email provider id: "+ ui.getProviderId());
//                Intent intent = new Intent(MainActivity.this, SuccessfulActivity.class);
//                if(ui.getProviderId().equals("password")){
//                    if(currentUser.isEmailVerified()){
//                        startActivity(intent);
//                        finish();
//                    }
//                }else if(ui.getProviderId().equals("phone") || ui.getProviderId().equals("google.com")) {
//                    startActivity(intent);
//                    finish();
//                }
//            }
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.d("TAG", "Google_sign_in: failed" + e);
            }
        } else {
            Log.d("TAG", "fb_auth_log: onActivityResult: " + requestCode);
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_birthday", "public_profile"));
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        //Sign in completed
                        Log.d("TAG", "fb_auth_log: success: logged in successfully");
                        //handling the token for Firebase Auth
                        handleFacebookAccessToken(loginResult.getAccessToken());

                        //Getting the user information
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), (object, response) -> {
                            // Application code
                            Log.d("TAG", "fb_auth_log: onCompleted: response: " + response.toString());
//                            try {
//                                String email = object.getString("email");
//                                String birthday = object.getString("birthday");
//
//                                Log.d("TAG", "fb_auth_log: onCompleted: Email: " + email);
//                                Log.d("TAG", "fb_auth_log: onCompleted: Birthday: " + birthday);
//
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                                Log.d("TAG", "fb_auth_log: onCompleted: JSON exception");
//                            }
                        });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday,photo");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("TAG", "fb_auth_log: facebook:onCancel");
//                        AccessToken accessToken = AccessToken.getCurrentAccessToken();
//                        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
//                        Log.d("TAG", "onCancel: "+ isLoggedIn);
//                        if(isLoggedIn){
//                            mAuth.signOut();
//                            LoginManager.getInstance().logOut();
//                            LoginManager.getInstance().logInWithReadPermissions(MainActivity.this, Arrays.asList("user_birthday","public_profile"));
//                        }
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Log.d("TAG", "fb_auth_log: facebook:onError", error);
                    }
                });
    }

    //google sign up
    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {

                        // Sign in success, update UI with the signed-in user's information
                        Log.d("TAG", "Google_sign_in: success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        user.getIdToken(true).addOnSuccessListener(result -> {
                            String mUserToken = result.getToken();
                            //Do whatever
                            Log.d("TAG", "GetTokenResult result = " + mUserToken);
                            JSONObject jsObj = new JSONObject();
                            try {
                                jsObj.put("name", user.getDisplayName());
                                jsObj.put("email", user.getEmail());
                                jsObj.put("phone", user.getPhoneNumber());
                                jsObj.put("token", mUserToken);
                                jsObj.put("photo", user.getPhotoUrl());
                                jsObj.put("uid", user.getUid());
                            } catch (Exception e) {

                            }
                            Intent intent = new Intent(MainActivity.this, PasswordSettingsActivity.class);
                            intent.putExtra("userdata", jsObj.toString());
                            startActivity(intent);
                            finish();
                        });

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("TAG", "Google_sign_in: failure", task.getException());
                    }
                });
    }
}