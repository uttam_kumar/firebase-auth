package com.uttam.phoneauthtest;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

//import com.google.android.gms.auth.api.signin.GoogleSignIn;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SuccessfulActivity extends AppCompatActivity {

    private TextView welcomeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successful);
        welcomeText = findViewById(R.id.welcomeTextId);
        displayInfo();
        Button logoutBtn = findViewById(R.id.logoutBtnId);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(SuccessfulActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void displayInfo() {
//        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
//        if (acct != null) {
//            String personName = acct.getDisplayName();
//            String personGivenName = acct.getGivenName();
//            String personFamilyName = acct.getFamilyName();
//            String personEmail = acct.getEmail();
//            String personId = acct.getId();
//            Uri personPhoto = acct.getPhotoUrl();
//        }

        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        assert mUser != null;
        String phone = mUser.getPhoneNumber();
        String name = mUser.getDisplayName();
        String email = mUser.getEmail();
        Uri photoUrl = mUser.getPhotoUrl();
        String uid = mUser.getUid();
        welcomeText.setText("Name: "+ name +"\nPhone: "+ phone+"\nEmail: " + email);
        Log.d("TAG", "user_info: name: "+ name+ ", email: " +email + ", phone: "+ phone+ ", photo: "+ photoUrl );
        Log.d("TAG", "user_info uid: " + uid);
        Log.d("TAG", "user_info: token " + mUser.getIdToken(true));
    }
}