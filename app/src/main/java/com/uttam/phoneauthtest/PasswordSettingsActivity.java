package com.uttam.phoneauthtest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.util.HashMap;
import java.util.Map;

public class PasswordSettingsActivity extends AppCompatActivity {

    private EditText editPassword, editConfPassword;
    private TextView errorText;
    private String password = "";
    private String first_name = "",last_name="", phone=null, email=null, photo= null, token="", uid="";
    private String dob = null, gender=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_settings_axtivity);

        editPassword = findViewById(R.id.editPasswordId);
        editConfPassword = findViewById(R.id.editConfPasswordId);

        errorText = findViewById(R.id.errorPassTextId);
    }


    public void setPasswordBtnClick(View v){
        password = editPassword.getText().toString();
        String confPass = editConfPassword.getText().toString();
        if(password.length() == 0){
            errorText.setText("Password field is empty.");
        }else if(password.length() < 6){
            errorText.setText("Weak password.");
        } else if(confPass.length() == 0){
            errorText.setText("Confirm Password field is empty.");
        }else{
            if(password.equals(confPass)){
                sendPostRequest();
                errorText.setText("set password successful, here will the query.");
            }else{
                errorText.setText("Password mismatch.");
            }
        }
    }

    private String getData(JSONObject mainObject, String key){
        Object data = JSONObject.NULL;
        try {
            if(!mainObject.getString(key).equals("")){
                data = mainObject.getString(key);
            }
        }catch (Exception e){

        }
        return data.toString();
    }

    //post request for signup
    private void sendPostRequest() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        Log.d("TAG", "sendPostRequest: "+ user.toString());
        OkHttpHandler okHttpHandler = new OkHttpHandler();
        JSONObject obj = new JSONObject();
        String userdata = getIntent().getStringExtra("userdata");
        Log.d("TAG", "sendPostRequest: "+ userdata);
        JSONObject mainObject = null;
        try {
            mainObject = new JSONObject(userdata);

////                jsObj.put("token", mUserToken);
//
//            jsObj.put("photo", null);
//            jsObj.put("dob", null);
//            jsObj.put("gender", null);
//            jsObj.put("uid", user.getUid());
            first_name = getData(mainObject, "first_name");
            last_name = getData(mainObject, "last_name");
            phone = getData(mainObject, "phone");
            email = getData(mainObject, "email");
            photo = getData(mainObject, "photo");
            dob = getData(mainObject, "dob");
            gender = getData(mainObject, "gender");
//            token = getData(mainObject, "token");
            uid = getData(mainObject, "uid");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            obj.put("first_name", first_name);
            obj.put("last_name", last_name);
            obj.put("dob", dob);
            obj.put("gender", gender);
            obj.put("phone", phone);
            obj.put("email", email);
            obj.put("photo", photo);
//            obj.put("token", token);
            obj.put("uid", uid);
            obj.put("password", password);
        }catch (Exception e){

        }

        String json = obj.toString();
        String url = "https://tmp.getsbo.com:13443/signup";
        Log.d("TAG", "sendPostRequest: "+json);
        new Thread(() -> {
            try {
                String response = okHttpHandler.requestPOST(url, json);
                Log.d("TAG", "handleSignIn: "+response);
            } catch (Exception e) {
                Log.d("TAG", "handleSignIn: e "+ e.getMessage());
            }
        }).start();
    }

}