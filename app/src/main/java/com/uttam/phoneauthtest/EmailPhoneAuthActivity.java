package com.uttam.phoneauthtest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class EmailPhoneAuthActivity extends AppCompatActivity {

    private static final int STATE_CODE_SENT = 301;
    private static final int STATE_PHONE_INVALID = 302;
    private static final int STATE_CODE_INVALID = 303;


    private static final String STATIC_PASSWORD = "123456";

    private FirebaseAuth mAuth;
    private String mVerificationId = "", phoneNo=null, emailId=null, first_name="", last_name="";
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private EditText editFirstName, editLastName, editPhone, editEmail, editVcode;
    private TextView initialError, statusText;
    private String SIGN_IN_OPTION = "";
    private LinearLayout initialLayout, secondartLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_phone_auth);

        mAuth = FirebaseAuth.getInstance();

        SIGN_IN_OPTION = getIntent().getStringExtra("sign_in_option");
        Log.d("TAG", "onCreate_sign_in_option: "+SIGN_IN_OPTION);


        editFirstName = findViewById(R.id.editFirstNameId);
        editLastName = findViewById(R.id.editLastNameId);
        editPhone = findViewById(R.id.editPhoneNoId);
        editEmail = findViewById(R.id.editEmailId);
        initialError = findViewById(R.id.initialErrorTextId);
        statusText = findViewById(R.id.statusTextId);

        initialLayout = findViewById(R.id.initialLayoutId);
        secondartLayout = findViewById(R.id.secondartLayoutId);

        editVcode = findViewById(R.id.editVcodeId);

        if(SIGN_IN_OPTION.equals("phone")){
            editEmail.setVisibility(View.GONE);
            editPhone.setVisibility(View.VISIBLE);
            secondartLayout.setVisibility(View.GONE);
        }else{
            editEmail.setVisibility(View.VISIBLE);
            editPhone.setVisibility(View.GONE);
            secondartLayout.setVisibility(View.GONE);
        }
    }

    public void handleResend(View v){
        if(SIGN_IN_OPTION.equals("phone"))
            resendVerificationCode(mResendToken );
        else{
            FirebaseUser user = mAuth.getCurrentUser();
//            mAuth.signOut();
//            user.delete();
//            anonymousSignin();
        }
    }

    private void goToPasswordSettingsActivity(){
        FirebaseUser user = mAuth.getCurrentUser();
        JSONObject jsObj = new JSONObject();

//        user.getIdToken(true).addOnSuccessListener(result -> {
//            String mUserToken = result.getToken();
            try{
                jsObj.put("first_name", first_name);
                jsObj.put("last_name", last_name);
                jsObj.put("email", emailId);
                jsObj.put("phone", phoneNo);
//                jsObj.put("token", mUserToken);

                jsObj.put("photo", null);
                jsObj.put("dob", null);
                jsObj.put("gender", null);
                jsObj.put("uid", user.getUid());
            }catch(Exception e){

            }
            Intent intent = new Intent(EmailPhoneAuthActivity.this, PasswordSettingsActivity.class);
            intent.putExtra("userdata", jsObj.toString());
            startActivity(intent);
            finish();
//        });
    }

    public void handleConfirm(View v){
        if(SIGN_IN_OPTION.equals("phone")){
            String vCode = editVcode.getText().toString();
            verifyPhoneNumberWithCode(mVerificationId, vCode);
        }else{
            FirebaseUser user = mAuth.getCurrentUser();
            user.reload().addOnCompleteListener(task -> {
                if(user.isEmailVerified()){
                    statusText.setText("");
                    goToPasswordSettingsActivity();
                }else{
                    statusText.setText("verify first. then click confirm");
                }
            });
        }
    }

    public void handleNextBtnClick(View v){
        emailId = editEmail.getText().toString();
        phoneNo = editPhone.getText().toString();
        first_name = editFirstName.getText().toString();
        last_name = editLastName.getText().toString();
        Toast.makeText(EmailPhoneAuthActivity.this, first_name + last_name, Toast.LENGTH_SHORT).show();

        if(SIGN_IN_OPTION.equals("phone")) sendVerificationCode();
        else  emailPasswordSignIn();

    }

    private void sendVerificationEmail(){
        FirebaseUser user = mAuth.getCurrentUser();
        assert user != null;
//        user.reload().addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
                if(!user.isEmailVerified()) {
                    user.sendEmailVerification()
                            .addOnCompleteListener(task1 -> {
                                Log.d("TAG", "sendVerificationEmail: " + user.getEmail());
                                if (task1.isSuccessful()) {
                                    initialLayout.setVisibility(View.GONE);
                                    secondartLayout.setVisibility(View.VISIBLE);
                                    initialError.setText("");
                                    editVcode.setVisibility(View.GONE);
                                    statusText.setText("an Email is sent to your mail address. Please verify it and click confirm button");
                                } else {
                                    initialError.setText("Some thing went wrong: " + task1.getException());
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("TAG", "onFailure_email: "+ e.getMessage());
                        }
                    });
                }else{
                    initialError.setText("already verified");
                }
//            }
//        });

    }

    private void emailPasswordSignUp(){
        mAuth.createUserWithEmailAndPassword(emailId, STATIC_PASSWORD)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("TAG", "signInWithEmail: createUserWithEmail:success");
                        sendVerificationEmail();
                    } else {
                        initialError.setText("something went wrong");
                        // If sign in fails, display a message to the user.
                        Log.d("TAG", "signInWithEmail: createUserWithEmail:failure", task.getException());
                    }
                });
    }

    private void emailPasswordSignIn(){
        mAuth.signInWithEmailAndPassword(emailId, STATIC_PASSWORD)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = mAuth.getCurrentUser();
                        if(user.isEmailVerified()){
                            //need to update ui
                            goToPasswordSettingsActivity();
                        }else{
                            sendVerificationEmail();
                        }
                        Log.d("TAG", "signInWithEmail:success: " + user.getUid());
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("TAG", "signInWithEmail:failure", task.getException());
                        try {
                                throw task.getException();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                initialError.setText("The email address is badly formatted.");
                                editEmail.requestFocus();
                            } catch(FirebaseAuthInvalidUserException e) {
//                            initialError.setText("There is no user record corresponding to this identifier.");
//                                editEmail.requestFocus();
                                emailPasswordSignUp();
                            } catch(Exception e) {
                                Log.e("TAG", e.getMessage());
                            }
                    }
                });
    }

//    private void anonymousSignin(){
////        Toast.makeText(EmailPhoneAuthActivity.this, "anonymousSignin", Toast.LENGTH_SHORT).show();
//        mAuth.signInAnonymously()
//                .addOnCompleteListener(this, task -> {
//                    if (task.isSuccessful()) {
//                        FirebaseUser user = mAuth.getCurrentUser();
//                        user.updateEmail(emailId)
//                                .addOnCompleteListener(task1 -> {
//                                    if (task1.isSuccessful()) {
//                                        sendVerificationEmail();
//                                        Log.d("TAG", "User email address updated.");
//                                    }else{
//                                        initialError.setText("Some thing went wrong. " + task1.getException());
//                                        Log.d("TAG", "User email address updated. error"+ task1.getException());
//                                    }
//                                });
//                    } else {
//                        initialError.setText("Some thing went wrong. " +task.getException());
//                        // If sign in fails, display a message to the user.
//                        Log.d("TAG", "signInWithEmail:failure", task.getException());
////                            try {
////                                throw task.getException();
////                            } catch(FirebaseAuthInvalidCredentialsException e) {
////                                statusText.setText(getResources().getString(R.string.invalid_password));
////                                mEditPass.requestFocus();
////                            } catch(FirebaseAuthInvalidUserException e) {
////                                statusText.setText(getResources().getString(R.string.invalid_user));
////                                mEditEmail.requestFocus();
////                            } catch(Exception e) {
////                                Log.e("TAG", e.getMessage());
////                            }
//                    }
//                });
//    }

    private void sendVerificationCode() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNo, /*time duration*/90, TimeUnit.SECONDS, this, mCallbacks);
    }

    //resend verification code for sign in with phone number verification
    private void resendVerificationCode(PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNo, 90, TimeUnit.SECONDS, this, mCallbacks, token);
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        verifyCodeWithCredential(credential);
    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks =
            new PhoneAuthProvider.OnVerificationStateChangedCallbacks()  {
                @Override
                public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                    verifyCodeWithCredential(phoneAuthCredential);
                }

                @Override
                public void onVerificationFailed(@NonNull FirebaseException e) {
                    updateUI(STATE_PHONE_INVALID);
                }

                @Override
                public void onCodeSent(@NonNull String verificationId,
                                       @NonNull PhoneAuthProvider.ForceResendingToken token) {
                    mVerificationId = verificationId;
                    mResendToken = token;
                    Log.d("TAG", "onCodeSent: "+ token);
                    updateUI(STATE_CODE_SENT);
                }
            };

//    //sign in with phone number verification
    private void verifyCodeWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        goToPasswordSettingsActivity();
                    } else {
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            Log.d("TAG", "onComplete: Invalid code");
                            Toast.makeText(EmailPhoneAuthActivity.this, "invalid code", Toast.LENGTH_SHORT).show();
                        }
                        Toast.makeText(EmailPhoneAuthActivity.this, "invalid code", Toast.LENGTH_SHORT).show();
                        updateUI(STATE_CODE_INVALID);
                    }
                });
    }

    private void updateUI(int state){
        Log.d("TAG", "state-updateUI: " + state);
        switch (state) {
            case STATE_PHONE_INVALID:
                initialError.setText(getResources().getString(R.string.invalid_phone));
                break;
            case STATE_CODE_SENT:
                initialLayout.setVisibility(View.GONE);
                secondartLayout.setVisibility(View.VISIBLE);
                initialError.setText("");
                statusText.setText("Code sent");
                break;
            case STATE_CODE_INVALID:
                statusText.setText(getResources().getString(R.string.invalid_otp));
                break;
        }
    }
}